const
  app = (req, res) => {
    res.render(`app`);
  },

  error = (err, req, res) => {
    res.status(500);
    res.render(`500`, {error: err, title: `500: Internal Server Error`});
  },

  index = (req, res) => {
    res.render(`index`);
  },

  notFound = (req, res) => {
    res.status(404);
    res.render(`404`);
  },

  report = (req, res) => {
    res.render(`report`);
  };

export default {
  app,
  error,
  index,
  notFound,
  report
};

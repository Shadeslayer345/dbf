import bs from 'browser-sync';
import compression from 'compression';
import express from 'express';
import favicon from 'serve-favicon';
import stylus from 'stylus';

import routes from './routes.js';

const
  app = express(),
  env = process.env.NODE_ENV,
  port = (env === `production`) ? 3007 : 2368,
  viewPath = `${__dirname}/app/views`;

app.set(`views`, viewPath);
app.set(`view engine`, `pug`);
app.set(`port`, port);
app.use(compression());
app.use(favicon(`${__dirname}/app/assets/favicon/favicon.ico`));
app.use(stylus.middleware({
  src: `${__dirname}/app/styles`,
  dest: `${__dirname}/app/dist/css`,
  compress: true,
  compile: (str, path) => {
    return stylus(str)
        .set(`filename`, path)
        .set(`compress`, true);
  }
}));

app.get(`/`, routes.index);
app.get(`/app`, routes.app);
app.get(`/report`, routes.report);

app.use(express.static(`${__dirname}/app/dist`));

app.use(routes.notFound);
app.use(routes.error);

app.listen(port, () => {
  if (env === `production`) {
    console.log(`listening on port ${app.get(`port`)}`);
  } else {
    bs({
      files: [`app/views/*.pug`, `app/styles/*.styl`],
      open: false,
      proxy: `localhost:${app.get(`port`)}`
    });
    console.log(`listening on port ${app.get(`port`)}  w/ browser-sync`);
  }
});

# DBF (Database Final)

## Getting Started
Clone the repo:

``` bash
git clone https://bitbucket.org/Shadeslayer345/dbf

cd dbf
```

Install dependencies:

``` bash
npm install
```

Run the server:

``` bash
// Run server in production
npm run start

// Run server in dev mode (with live reloading)
npm run start:dev
```
